<table class="table">
    <thead>
        <tr>
            <th>Client</th>
            <th>Deal</th>
            <th>Hour</th>
            <th>Accepted</th>
            <th>Refused</th>
        </tr>
    </thead>
    <tbody>
        @forelse($transactions as $transaction)
            <tr>
                <td>{{ $transaction->client }}</td>
                <td>{{ $transaction->deal }}</td>
                <td>{{ \Carbon\Carbon::parse($transaction->hour)->format('Y-m-d H:i') }}</td>
                <td>{{ $transaction->accepted }}</td>
                <td>{{ $transaction->refused }}</td>
            </tr>
        @empty
            <tr>
                <td colspan="5">
                    <p class="font-weight-bold text-center">No data</p>
                </td>
            </tr>
        @endforelse
    </tbody>
</table>
