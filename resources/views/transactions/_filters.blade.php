<form method="get" action="{{ route('transactions.index') }}">
    <div class="row pb-3">
        <div class="col-md-6">
            <div class="form-inline">
                <div class="input-group">
                    <label for="client-id" class="mr-1">Client Id:</label>
                    <input id="client-id" pattern="\d*" type="text" name="clientid" value="{{ request('clientid') }}" class="form-control form-control-sm" placeholder="Client Id" autofocus>
                </div>
                <div class="input-group">
                    <label for="deal-id" class="mx-1">Deal Id:</label>
                    <input id="deal-id" pattern="\d*" type="text" name="dealid" value="{{ request('dealid') }}" class="form-control form-control-sm" placeholder="Deal Id">
                </div>
            </div>
        </div>
        <div class="col-md-6 text-right">
            <div class="form-inline justify-content-end">
                <div class="input-group">
                    <label for="from" class="mr-1">From:</label>
                    <input type="date" class="form-control form-control-sm" name="daterange[]" id="from" placeholder="From" value="{{ request('daterange')[0] }}">
                </div>
                <div class="input-group">
                    <label for="to" class="mx-1">To:</label>
                    <input type="date" class="form-control form-control-sm" name="daterange[]" id="to" placeholder="To" value="{{ request('daterange')[1] }}">
                </div>
                <button type="submit" class="btn btn-sm btn-primary mx-1">Filter</button>
                @if(request()->query->count() > 0)
                    <a href="{{ route('transactions.index') }}" class="btn btn-sm btn-danger">X</a>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="custom-control-inline">
                <label class="">Group by:</label>
            </div>
            @foreach(trans('transactions.groupBy') as $value)
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio"
                           id="group-by-{{ $value }}"
                           name="groupby"
                           value="{{ $value }}"
                           class="custom-control-input"
                        {{ $value == request('groupby') ? 'checked' : '' }}
                    >
                    <label class="custom-control-label" for="group-by-{{ $value }}">{{ \Illuminate\Support\Str::ucfirst($value) }}</label>
                </div>
            @endforeach
        </div>
    </div>
</form>
