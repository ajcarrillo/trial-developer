@extends('layouts.app')

@section('extra-head')
    <style>
        .t-col-pb {
            padding-bottom: 15px;
            padding-top: 15px;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col">
                @if(Session::has('message'))
                    <div class="alert alert-info alert-dismissible fade show" role="alert">
                        {{ Session::get('message') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col pb-3">
                <div class="card">
                    <div class="card-body">
                        <form method="POST" action="{{ route('import.cvs') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type='file' name='file'>
                            <button class="btn btn-primary" type="submit">Import</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col t-col-pb">
                <div class="card">
                    <div class="card-header">Transactions</div>
                    <div class="card-body pb-0">
                        @include('transactions._filters')
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">Transactions details</div>
                    <div class="card-body table-responsive">
                        @includeWhen(filters_are_empty(\Request::only(['groupby', 'clientid', 'dealid'])), 'transactions._all_transactions', ['transactions'=>$paginateTransactions])
                        @includeWhen((request('clientid', NULL) || request('dealid', NULL)) && !request('groupby', NULL), 'transactions._transactions_by_cliente_or_deal_table', ['transactions'=>$paginateTransactions])
                        @includeWhen(request('groupby', NULL) === 'hour', 'transactions._trasactions_groupby_hour_table', ['transactions'=>$paginateTransactions])
                        @includeWhen(request('groupby', NULL) === 'day', 'transactions._trasactions_groupby_day_table', ['transactions'=>$paginateTransactions])
                        @includeWhen(request('groupby', NULL) === 'month', 'transactions._trasactions_groupby_month_table', ['transactions'=>$paginateTransactions])
                    </div>
                    <hr>
                    <div class="card-body">
                        <div class="row">
                            <div class="col text-center">
                                <span class="d-block h4">{{ $transactions->sum('accepted') }}</span>
                                <span class="d-block h5">Accepted transactions</span>
                            </div>
                            <div class="col text-center">
                                <span class="d-block h4">{{ $transactions->sum('refused') }}</span>
                                <span class="d-block h5">Refused transactions</span>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        {{ $paginateTransactions->links() }}
                    </div>

                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col t-col-pb">
                <div class="card">
                    <div class="card-header">Grand total chart</div>
                    <div class="card-body">
                        {!! $grandTotalChart->container() !!}
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="row">
                    <div class="col-12 t-col-pb">
                        <div class="card">
                            <div class="card-header">Grand total accepted transactions</div>
                            <div class="card-body">
                                <h1 class="text-center">{{ $grandTotal->accepted }}</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 t-col-pb">
                        <div class="card">
                            <div class="card-header">Gran total refused transactions</div>
                            <div class="card-body">
                                <h1 class="text-center">{{ $grandTotal->refused }}</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col t-col-pb">
                @if(!is_null($groupByChart))
                    <div class="card">
                        @if(request('groupby'))
                            <div class="card-header">Transactions group by {{ request('groupby') }}</div>
                        @else
                            <div class="card-header">Transactions</div>
                        @endif
                        <div class="card-body">
                            {!! $groupByChart->container() !!}
                        </div>
                        <hr>
                        <div class="card-body">
                            <div class="row">
                                <div class="col text-center">
                                    <span class="d-block h2">{{ $transactions->sum('accepted') }}</span>
                                    <span class="d-block h4">Accepted transactions</span>
                                </div>
                                <div class="col text-center">
                                    <span class="d-block h2">{{ $transactions->sum('refused') }}</span>
                                    <span class="d-block h4">Refused transactions</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="card">
                        <div class="card-header">No data</div>
                        <div class="card-body">
                            Choose a filter...
                        </div>
                    </div>
                @endif
            </div>
        </div>

    </div>
@endsection

@section('extra-scrtips')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
    <script src="https://cdn.jsdelivr.net/gh/emn178/chartjs-plugin-labels/src/chartjs-plugin-labels.js" charset="utf-8"></script>
    {!! $grandTotalChart->script() !!}
    @if(!is_null($groupByChart))
        {!! $groupByChart->script() !!}
    @endif
    <script>
        Chart.defaults.global.plugins.labels = {
            fontSize: 14,
            render: 'value'
        };
    </script>
@endsection
