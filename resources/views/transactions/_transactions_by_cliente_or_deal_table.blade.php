<table class="table">
    <thead>
        <tr>
            @include('transactions._head_client_or_deal')
            <th>Accepted</th>
            <th>Refused</th>
        </tr>
    </thead>
    <tbody>
        @forelse($transactions as $transaction)
            <tr>
                @include('transactions._row_client_or_deal')
                <td>{{ $transaction->accepted }}</td>
                <td>{{ $transaction->refused }}</td>
            </tr>
        @empty
            <tr>
                <td colspan="5">
                    <p class="font-weight-bold text-center">No data</p>
                </td>
            </tr>
        @endforelse
    </tbody>
</table>
