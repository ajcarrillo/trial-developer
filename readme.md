# Trial developer

## Creación de base de datos

```mysql
DROP DATABASE IF EXISTS trialdeveloper;
CREATE DATABASE trialdeveloper
    CHARACTER SET = 'utf8mb4'
    COLLATE = 'utf8mb4_unicode_ci';
 ```

### Configuración de la conexión a la BD
 
 Configurar en el archivo `.env` lo siguiente:
 
 ``` 
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=your_port
DB_DATABASE=trialdeveloper
DB_USERNAME=your_username
DB_PASSWORD=your_password 
```

## Creación de tablas y usuario de prueba

Para crear las tablas y el usuario de pruebas ejecuta el siguiente comando:

`$ php artisan migrate:fresh --seed`

user: `trial`
pass: `password`

## Importar archivo CSV

Puedes hacerlo de dos maneras desde consola y desde el frontend

Desde consola ejecuta el siguiente comando:

`$ php artisan import:csv`

Desde la interfaz de usuario:

Ingresa a la siguiente url: `/transacciones` e importa el archivo que se encuenta en
`public/csv/trial.csv`
