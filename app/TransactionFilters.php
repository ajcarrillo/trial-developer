<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 4/27/20
 * Time: 10:25 p. m.
 */

namespace App;


use App\Classes\QueryFilter;

class TransactionFilters extends QueryFilter
{

    public function rules(): array
    {
        return [
            'daterange' => 'filled',
            'clientid'  => 'filled',
            'dealid'    => 'filled',
            'groupby'   => 'filled|in:hour,day,month',
        ];
    }

    public function groupby($query, $groupby)
    {
        $this->$groupby($query);
    }

    protected function hour($query)
    {
        $query->selectRaw('hour(transactions.hour) as hour')
            ->groupByRaw('hour(transactions.hour)');
    }

    protected function day($query)
    {
        $query->selectRaw('date(transactions.hour) as dayname')
            ->groupByRaw('date(transactions.hour)')
            ->orderByRaw('date(transactions.hour)');
    }

    protected function month($query)
    {
        $query->selectRaw('monthname(transactions.hour) as monthname')
            ->groupByRaw('monthname(transactions.hour)');
    }

    public function clientid($query, $clientid)
    {
        $query->selectRaw('transactions.client_id, c.name as client')
            ->where('transactions.client_id', $clientid)
            ->groupByRaw('transactions.client_id, c.name');
    }

    public function dealid($query, $dealid)
    {
        $query->selectRaw('transactions.deal_id, d.name as deal')
            ->where('transactions.deal_id', $dealid)
            ->groupByRaw('transactions.deal_id, d.name');
    }

    public function daterange($query, $daterange)
    {
        if ( ! is_null($daterange[0]) or ! is_null($daterange[1])) {

            $daterange[0] = is_null($daterange[0]) ? $daterange[1] : $daterange[0];
            $daterange[1] = is_null($daterange[1]) ? $daterange[0] : $daterange[1];

            $query->whereRaw('date(transactions.hour) BETWEEN ? AND ?', [ $daterange[0], $daterange[1] ]);
        }
    }
}
