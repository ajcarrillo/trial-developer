<?php

namespace App\Console\Commands;

use App\Transaction;
use DB;
use Illuminate\Console\Command;
use Schema;

class ImportCSVFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:csv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importa el archivo csv';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Truncando tabla transactions');
        Schema::disableForeignKeyConstraints();
        DB::table('transactions')->truncate();
        DB::table('clients')->truncate();
        DB::table('deals')->truncate();
        Schema::enableForeignKeyConstraints();
        $this->info('Se truncó la tabla transactions');

        $this->info('Importando archivo csv, espere un momento...');
        Transaction::importCSV('trial.csv', 'csv');
        $this->info('El archivo se importó correctamente');
    }
}
