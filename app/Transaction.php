<?php

namespace App;

use App\Classes\QueryFilter;
use DB;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'client_id',
        'deal_id',
        'hour',
        'accepted',
        'refused',
    ];

    public static function importCSV($filename, $folder = 'uploads')
    {
        $filepath = public_path("{$folder}/{$filename}");

        $file = fopen($filepath, "r");

        $iteration = 0;

        while ($data = self::processFile($file, $iteration)) {
            DB::table('transactions')->insert($data);
            $iteration++;
        }
        fclose($file);
    }

    protected static function processFile($file, $iteration)
    {
        $data      = [];
        $clientIds = [];
        $dealsIds  = [];

        $lineCount = 0;

        while ($lineCount < 100 && ($row = fgetcsv($file, 1000, ',')) !== FALSE) {
            $countColumns = count($row);

            if ($iteration == 0) {
                if ($lineCount == 0) {
                    $lineCount++;
                    continue;
                }
            }

            $values = [];

            $values[] = self::getClientId($row);
            $values[] = self::getDealId($row);

            for ($column = 2; $column < $countColumns; $column++) {
                array_push($values, $row [ $column ]);
            }

            $clientIds[] = array_combine([ 'id', 'name' ], [ self::getClientId($row), self::getClientName($row) ]);
            $dealsIds[]  = array_combine([ 'id', 'name' ], [ self::getDealId($row), self::getDealName($row) ]);

            $data[] = array_combine([
                'client_id',
                'deal_id',
                'hour',
                'accepted',
                'refused',
            ], $values);

            DB::table('clients')->insertOrIgnore($clientIds);
            DB::table('deals')->insertOrIgnore($dealsIds);

            $lineCount++;
        }

        return $data;
    }

    protected static function getClientId($row)
    {
        return explode('@', $row[0])[1];
    }

    protected static function getClientName($row)
    {
        return rtrim(explode('@', $row[0])[0]);
    }

    protected static function getDealId($row)
    {
        return explode('#', $row[1])[1];
    }

    protected static function getDealName($row)
    {
        return rtrim(explode('#', $row[1])[0]);
    }

    public function scopeFilterBy($query, QueryFilter $filters, array $data)
    {
        return $filters->applyTo($query, $data);
    }
}
