<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 4/27/20
 * Time: 9:15 p. m.
 */

namespace App\Http\Controllers;


use App\Transaction;
use DB;
use Illuminate\Http\Request;
use Schema;
use Session;

class ImportCSVController extends Controller
{
    public function store(Request $request)
    {
        Schema::disableForeignKeyConstraints();
        DB::table('transactions')->truncate();
        Schema::enableForeignKeyConstraints();

        $file = $request->file('file');

        $filename  = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();

        $valid_extension = array( "csv" );

        if (in_array(strtolower($extension), $valid_extension)) {
            $location = 'uploads';

            $file->move($location, $filename);

            Transaction::importCSV($filename);

            Session::flash('message', 'El archivo se importó correctamente');
        } else {
            Session::flash('message', 'Tipo de archivo no admitido');
        }

        return back();
    }
}
