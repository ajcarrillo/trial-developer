<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 4/27/20
 * Time: 9:22 p. m.
 */

namespace App\Http\Controllers;


use App\Charts\AcceptedVRefused;
use App\Transaction;
use App\TransactionFilters;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class TransactionController extends Controller
{
    public function index(Request $request, TransactionFilters $filters)
    {
        $values = $this->getValues($request);

        $grandTotal      = $this->getGrandTotal();
        $grandTotalChart = $this->getGrandtotalPieChart($grandTotal);

        $items        = $this->getData($filters, $values);
        $groupByChart = $this->processGroupByChart($request, $items);

        $paginateTransactions = $this->getTransactionsTableData($filters, $values, $request);

        return view('transactions.index', [
            'transactions'         => $items,
            'grandTotal'           => $grandTotal,
            'groupByChart'         => $groupByChart,
            'grandTotalChart'      => $grandTotalChart,
            'paginateTransactions' => $paginateTransactions,
        ]);
    }

    protected function getValues(Request $request): array
    {
        return $request->only([
            'daterange',
            'clientid',
            'dealid',
            'groupby',
        ]);
    }

    protected function getQuery()
    {
        return Transaction::query()
            ->join('clients as c', 'transactions.client_id', '=', 'c.id')
            ->join('deals as d', 'transactions.deal_id', '=', 'd.id')
            ->selectRaw('sum(transactions.accepted) as accepted')
            ->selectRaw('sum(transactions.refused) as refused');
    }

    protected function getGrandTotal()
    {
        return $this->getQuery()
            ->first();
    }

    protected function getGrandtotalPieChart($data)
    {
        $chart = new AcceptedVRefused;

        $chart->labels([ "Accepted", "Refused" ]);
        $chart->dataset('Transactions', 'pie', [
            $data->accepted,
            $data->refused,
        ])->options([
            'backgroundColor' => [ '#36A2EB', '#FF6384' ],
        ]);

        $chart->displayAxes(false);

        return $chart;
    }

    protected function getHourChart($data)
    {
        $labels = $data->map(function ($el) {
            return "{$el->hour} hrs";
        });

        return $this->generateChart($data, $labels);
    }

    protected function validateGroupBy(Request $request)
    {
        $request->validate([
            'groupby' => 'filled|in:hour,day,month',
        ]);
    }

    protected function getDayChart($data)
    {
        $labels = $data->map(function ($el) {
            return $el->dayname;
        });

        return $this->generateChart($data, $labels);
    }

    protected function getMonthChart($data)
    {
        $labels = $data->map(function ($el) {
            return $el->monthname;
        });

        return $this->generateChart($data, $labels);
    }

    protected function generateChart($data, $labels, $type = 'bar'): AcceptedVRefused
    {
        $accepted = $data->map(function ($el) {
            return $el->accepted;
        });

        $refused = $data->map(function ($el) {
            return $el->refused;
        });

        $chart = new AcceptedVRefused;

        $chart->labels($labels);
        $chart->dataset('Accepted', $type, $accepted)
            ->options([ 'backgroundColor' => '#36A2EB', 'borderColor' => '#36A2EB' ]);
        $chart->dataset('Refused', $type, $refused)
            ->options([ 'backgroundColor' => '#FF6384', 'borderColor' => '#FF6384' ]);

        return $chart;
    }

    protected function getData(TransactionFilters $filters, array $values)
    {
        return Transaction::query()
            ->join('clients as c', 'transactions.client_id', '=', 'c.id')
            ->join('deals as d', 'transactions.deal_id', '=', 'd.id')
            ->filterBy($filters, $values)
            ->selectRaw('sum(transactions.accepted) as accepted')
            ->selectRaw('sum(transactions.refused) as refused')
            ->get();
    }

    protected function processGroupByChart(Request $request, $items)
    {
        $groupByChart = NULL;

        if (count($items) > 0) {
            if ($request->has('groupby')) {
                $this->validateGroupBy($request);

                $groupby = Str::ucfirst($request->query('groupby'));
                $method  = "get{$groupby}Chart";

                if (method_exists($this, $method)) {
                    $groupByChart = $this->$method($items);
                    $groupByChart->title($this->getTitleChart($request));
                }
            } else {
                $validator = Validator::make($request->only('clientid', 'dealid'), [
                    'clientid' => 'required_without:dealid',
                    'dealid'   => 'required_without:clientid',
                ]);

                if ( ! $validator->fails()) {
                    $groupByChart = new AcceptedVRefused;

                    $groupByChart->labels([ "Accepted", "Refused" ]);
                    $groupByChart->dataset('Transactions', 'pie', [
                        $items[0]['accepted'],
                        $items[0]['refused'],
                    ])->options([
                        'backgroundColor' => [ '#36A2EB', '#FF6384' ],
                    ]);

                    $groupByChart->displayAxes(false);
                    $groupByChart->title($this->getTitleChart($request));
                }
            }
        }

        return $groupByChart;
    }

    protected function getTitleChart(Request $request)
    {
        $title = '';

        if ($request->query('clientid')) {
            $client = DB::table('clients')->where('id', $request->query('clientid'))->first();
            $title  = "{$client->name}'s client transactions";
        }

        if ($request->query('dealid')) {
            $deal = DB::table('deals')->where('id', $request->query('dealid'))->first();
            if ($title) {
                $title = "{$title} from {$deal->name} deal";
            } else {
                $title = "{$deal->name}'s deal transactions";
            }
        }

        return $title;
    }

    protected function getTransactionsTableData(TransactionFilters $filters, array $values, Request $request)
    {
        $query = Transaction::query()
            ->join('clients as c', 'transactions.client_id', '=', 'c.id')
            ->join('deals as d', 'transactions.deal_id', '=', 'd.id')
            ->filterBy($filters, $values);

        if (filters_are_empty($request->only([ 'groupby', 'clientid', 'dealid' ]))) {
            $query->select('c.name as client', 'd.name as deal', 'hour', 'accepted', 'refused');
        } else {
            $query->selectRaw('sum(transactions.accepted) as accepted')
                ->selectRaw('sum(transactions.refused) as refused');
        }

        return $query->paginate(15)->appends($request->all());
    }
}
