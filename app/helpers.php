<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 5/10/20
 * Time: 2:09 p. m.
 */

if ( ! function_exists('filters_are_empty')) {
    function filters_are_empty(array $filters)
    {
        $validator = Validator::make($filters, [
            'clientid' => 'filled',
            'dealid'   => 'filled',
            'groupby'  => 'filled',
        ]);
        \Log::info(count($validator->valid()) > 0 ? false : true);

        return count($validator->valid()) > 0 ? false : true;
    }
}
