<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\ImportCSVController;
use App\Http\Controllers\TransactionController;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes([
    'register' => false,
    'reset'    => false,
    'confirm'  => false,
    'verify'   => false,
]);


Route::get('/home', 'HomeController@index')->name('home');

Route::get('/transactions', [ TransactionController::class, 'index' ])
    ->middleware([ 'auth' ])
    ->name('transactions.index');

Route::post('/import-csv', [ ImportCSVController::class, 'store' ])
    ->middleware([ 'auth' ])
    ->name('import.cvs');
